const http = require('http')

const getRandom = (min, max) => Math.random() * (max - min) + min
const shouldFail = () => Math.random() > 0.8

const currencies = ['PLN', 'USD', 'EUR', 'GBP']

const getPrice = () => {
	const currencyId = Math.floor(getRandom(0, currencies.length))

	return {
		amount: getRandom(1, 100).toFixed(2),
		currency: currencies[currencyId],
	}
}

const port = process.env.PORT || 9010

http
	.createServer((req, res) => {
		const latency = Math.floor(getRandom(0, 500))
		setTimeout(() => {
			if (req.method !== 'GET' || !/^\/pricing\/.*$/.test(req.url)) {
				console.log(latency, 'Not Found', req.url)
				res.writeHead(404)
				res.end('Not Found')
			} else if (shouldFail()) {
				console.log(latency, 'Failure!')
				res.writeHead(500)
				res.end('System Downtime')
			} else {
				const price = getPrice()
				console.log(latency, price)
				res.writeHead(200, { 'Content-Type': 'application/json' })
				res.end(JSON.stringify(price))
			}
		}, latency)
	})
	.listen(port, () => {
		console.info('Pricing server started on port: ' + port)
	})
