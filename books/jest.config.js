module.exports = {
	moduleDirectories: ['node_modules', '<rootDir>/src'],
	testPathIgnorePatterns: [
		'<rootDir>/test',
		'<rootDir>/src/config/env',
		'<rootDir>/lib',
		'/node_modules/',
	],
	testEnvironment: 'node',
}
