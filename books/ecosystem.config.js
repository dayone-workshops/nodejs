module.exports = {
	apps: [
		{
			name: 'd1-nodejs',
			cwd: './',
			script: 'server.js',
			instances: 2,
			autorestart: true,
			watch: false,
			max_memory_restart: '600M',
			port: 9000,
			env: {
				NODE_ENV: 'production',
			},
		},
	],
}
