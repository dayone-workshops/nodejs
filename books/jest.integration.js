module.exports = {
	moduleDirectories: ['node_modules', '<rootDir>/src', '<rootDir>/test'],
	testPathIgnorePatterns: [
		'<rootDir>/src',
		'<rootDir>/src/config/env',
		'<rootDir>/lib',
		'/node_modules/',
	],
	setupFilesAfterEnv: ['<rootDir>/test/__setup.js'],
	testEnvironment: 'node',
}
