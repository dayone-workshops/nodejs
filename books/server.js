//Allow absolute path require
require('app-module-path').addPath(`${__dirname}/src`)

const logger = require('utils/logger')
logger.info(`Running in ${process.env.NODE_ENV} mode`)

require('./src/main')
