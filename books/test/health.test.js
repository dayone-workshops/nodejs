const request = require('supertest')
const app = require('main')

describe('Health API', () => {
	test('Is welcoming', done => {
		request(app)
			.get('/hello')
			.expect(200)
			.expect('World!')
			.end(done)
	})
})
