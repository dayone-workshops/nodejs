const express = require('express')
const logger = require('utils/logger')

const port = process.env.PORT || 9000
const app = express()

app.get('/hello', function(req, res) {
	res.send('World!')
})

const server = app.listen(port, () => {
	if (app.get('env') === 'test') return
	logger.info(`Server running on port: ${port}`)
})

module.exports = server
