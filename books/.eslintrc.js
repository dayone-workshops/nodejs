module.exports = {
	env: {
		browser: false,
		es6: true,
		node: true,
		'jest/globals': true,
	},
	extends: [
		'eslint:recommended',
		'plugin:node/recommended',
		'plugin:jest/recommended',
	],
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 2018,
	},
	plugins: ['jest', 'prettier', 'node'],
	rules: {
		indent: 0,
		'linebreak-style': ['error', 'unix'],
		semi: ['error', 'never'],
		'no-console': 'warn',
		'no-extra-boolean-cast': 'off',
		'no-mixed-spaces-and-tabs': 0,
		'prettier/prettier': 'warn',
		'node/no-missing-require': [
			'error',
			{
				resolvePaths: ['./src'],
			},
		],
	},
	overrides: [
		{
			files: ['test/**'],
			rules: {
				'node/no-unpublished-require': [
					'error',
					{
						allowModules: ['supertest'],
					},
				],
			},
		},
	],
}
