const http = require('http')
const books = require('./books/src/data/books.json')

const { floor } = Math
const getRandom = (min, max) => Math.random() * (max - min) + min
const shouldFail = () => Math.random() > 0.8

const getRecommendations = maxCount => {
	const res = []
	const count = floor(getRandom(0, maxCount))

	while (res.length < count) {
		const bookIdx = floor(getRandom(0, books.length))
		const book = books[bookIdx]
		res.push({
			id: book.id,
			title: book.title,
			score: getRandom(0.1, 1).toFixed(2),
		})
	}
	return res
}

const port = process.env.PORT || 9020
const MAX_RECOMMENDATIONS = 8

http
	.createServer((req, res) => {
		const latency = floor(getRandom(0, 1000))
		setTimeout(() => {
			if (req.method !== 'GET' || !/^\/recommend\/.*$/.test(req.url)) {
				console.log(latency, 'Not Found', req.url)
				res.writeHead(404)
				res.end('Not Found')
			} else if (shouldFail()) {
				console.log(latency, 'Failure!')
				res.writeHead(500)
				res.end('System Downtime')
			} else {
				const recommendations = getRecommendations(MAX_RECOMMENDATIONS)
				console.log(latency, recommendations)
				res.writeHead(200, { 'Content-Type': 'application/json' })
				res.end(JSON.stringify(recommendations))
			}
		}, latency)
	})
	.listen(port, () => {
		console.info('Recommendations server started on port: ' + port)
	})
